#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void show_menu_main(void);
void show_menu_sub(int menu);

typedef struct MusicData {
    char artist[30];
    char title[50];
    char genre[30];
    unsigned int year;
    double rating;
    unsigned int vote;
} MusicData;

typedef struct MusicDataList {
    MusicData *list;
    unsigned int size;
} MusicDataList;

MusicData make_music_data(char *artist, char *title, char *genre, unsigned int year, double rating, unsigned int vote) {
    MusicData d;
    strcpy(d.artist, artist);
    strcpy(d.title, title);
    strcpy(d.genre, genre);
    d.year = year;
    d.rating = rating;
    d.vote = vote;
    return d;
}

void init_music_data_list(MusicDataList *l) {
    l->list = (MusicData *)malloc(sizeof(MusicData) * 1);
    l->size = 0;
    
    if(l->list == NULL) {
        printf("Memory Allocation Error!\n");
        exit(1);
    }
}

void add_data_struct(MusicDataList *l, MusicData data) {
    l->list[l->size] = data;
    l->size++;
    l->list = (MusicData *)realloc(l->list, sizeof(MusicData) * (l->size + 1));
}

void add_default(MusicDataList *l) {
    add_data_struct(l, make_music_data("PSY", "Gentleman", "Dance", 2013, 8.72, 710));
    add_data_struct(l, make_music_data("Hyo_ri", "Bad_Girls", "Dance", 2013, 9.99, 331));
    add_data_struct(l, make_music_data("Yong_pil", "Bounce", "Ballad", 2013, 8.79, 88));
    add_data_struct(l, make_music_data("Roy_Kim", "BomBomBom", "Rock", 2013, 6.99, 29));
    add_data_struct(l, make_music_data("EXO", "Wolf", "Dance", 2013, 5.11, 30));
    add_data_struct(l, make_music_data("PSY", "GangnamSt", "Dance", 2012, 8.11, 952));
    add_data_struct(l, make_music_data("Freestyle", "Y", "Rap", 2005, 8.47, 152));
    add_data_struct(l, make_music_data("Jason_Mraz", "Im_Yours", "Pop", 2011, 7.52, 761));
    add_data_struct(l, make_music_data("Beatles", "Yesterday", "Pop", 1980, 6.59, 952));
}

void print_all(MusicDataList l) {
    int i;
    
    printf("\n\n");
    for(i = 0; i < l.size; i++) {
        printf("%3d - %-10s %-10s %-7s %5d %-4.2lf %4d\n",
               i+1, l.list[i].artist, l.list[i].title, l.list[i].genre, l.list[i].year, l.list[i].rating, l.list[i].vote);
    }
    printf("amount : %d\n", l.size);
}

void swap_music_data(MusicData *d1, MusicData *d2) {
    MusicData temp;
    
    if(d1 == d2) return;
    
    strcpy(temp.artist, d1->artist);
    strcpy(temp.title, d1->title);
    strcpy(temp.genre, d1->genre);
    temp.year = d1->year;
    temp.rating = d1->rating;
    temp.vote = d1->vote;
    
    strcpy(d1->artist, d2->artist);
    strcpy(d1->title, d2->title);
    strcpy(d1->genre, d2->genre);
    d1->year = d2->year;
    d1->rating = d2->rating;
    d1->vote = d2->vote;
    
    strcpy(d2->artist, temp.artist);
    strcpy(d2->title, temp.title);
    strcpy(d2->genre, temp.genre);
    d2->year = temp.year;
    d2->rating = temp.rating;
    d2->vote = temp.vote;
}

void sort_artist_as(MusicDataList l) {
    int i, j, min;
    int n = l.size;
    
    for (i = 0; i < n - 1; i++) {
        min = i;
        for (j = i + 1; j < n; j++) {
            if(strcmp(l.list[j].artist, l.list[min].artist) < 0) {
                min = j;
            }
        }
        swap_music_data(&l.list[min], &l.list[i]);
    }
}

void sort_title_as(MusicDataList l) {
    int i, j, min;
    int n = l.size;
    
    for (i = 0; i < n - 1; i++) {
        min = i;
        for (j = i + 1; j < n; j++) {
            if(strcmp(l.list[j].title, l.list[min].title) < 0) {
                min = j;
            }
        }
        swap_music_data(&l.list[min], &l.list[i]);
    }
}

void sort_genre_des(MusicDataList l) {
    int i, j, max;
    int n = l.size;
    
    for (i = 0; i < n - 1; i++) {
        max = i;
        for (j = i + 1; j < n; j++) {
            if(strcmp(l.list[j].genre, l.list[max].genre) > 0) {
                max = j;
            }
        }
        swap_music_data(&l.list[max], &l.list[i]);
    }
}

void sort_year_as(MusicDataList l) {
    int i, j, min;
    int n = l.size;
    
    for (i = 0; i < n - 1; i++) {
        min = i;
        for (j = i + 1; j < n; j++) {
            if(l.list[j].year < l.list[min].year) {
                min = j;
            }
        }
        swap_music_data(&l.list[min], &l.list[i]);
    }
}

void sort_rating_des(MusicDataList l) {
    int i, j, max;
    int n = l.size;
    
    for (i = 0; i < n - 1; i++) {
        max = i;
        for (j = i + 1; j < n; j++) {
            if(l.list[j].rating > l.list[max].rating) {
                max = j;
            }
        }
        swap_music_data(&l.list[max], &l.list[i]);
    }
}

void print_filter_artist(MusicDataList l, char *filter) {
    int size = l.size;
    int i;
    
    for(i = 0; i < size; i++) {
        if(strcmp(l.list[i].artist, filter) == 0) {
            printf("%-10s %-10s %-7s %5d %-4.2lf %4d\n",
                   l.list[i].artist, l.list[i].title, l.list[i].genre, l.list[i].year, l.list[i].rating, l.list[i].vote);
        }
    }
}

void print_filter_title(MusicDataList l, char *filter) {
    int size = l.size;
    int i;
    
    for(i = 0; i < size; i++) {
        if(strcmp(l.list[i].title, filter) == 0) {
            printf("%-10s %-10s %-7s %5d %-4.2lf %4d\n",
                   l.list[i].artist, l.list[i].title, l.list[i].genre, l.list[i].year, l.list[i].rating, l.list[i].vote);
        }
    }
}

void print_filter_genre(MusicDataList l, char *filter) {
    int size = l.size;
    int i;
    
    for(i = 0; i < size; i++) {
        if(strcmp(l.list[i].genre, filter) == 0) {
            printf("%-10s %-10s %-7s %5d %-4.2lf %4d\n",
                   l.list[i].artist, l.list[i].title, l.list[i].genre, l.list[i].year, l.list[i].rating, l.list[i].vote);
        }
    }
}

void add_rating(MusicDataList l, char *title) {
    int size = l.size;
    int i;
    unsigned int rating;
    
    for(i = 0; i < size; i++) {
        if(strcmp(l.list[i].title, title) == 0) {
            printf("Input Rating(0.00 ~ 10.00) : ");
            scanf("%d", &rating);
            l.list[i].rating *= l.list[i].vote;
            l.list[i].rating += rating;
            l.list[i].rating /= (l.list[i].vote + 1);
            l.list[i].vote++;
            return;
        }
    }
    
    printf("No Maching Title!\n");
}

int main(void) {
    MusicDataList data_list;
    MusicData data;
    int menu_main = 0, menu_sub = 0;
    char temp[100];
    
    init_music_data_list(&data_list);
    add_default(&data_list);
    
    while(menu_main != 5) {
        show_menu_main();
        scanf("%d", &menu_main);
        
        switch(menu_main) {
            case 1:
                show_menu_sub(menu_main);
                scanf("%d", &menu_sub);
                switch(menu_sub) {
                    case 1:
                        sort_artist_as(data_list);
                        print_all(data_list);
                        break;
                    case 2:
                        sort_title_as(data_list);
                        print_all(data_list);
                        break;
                    case 3:
                        sort_genre_des(data_list);
                        print_all(data_list);
                        break;
                    case 4:
                        sort_year_as(data_list);
                        print_all(data_list);
                        break;
                    case 5:
                        sort_rating_des(data_list);
                        print_all(data_list);
                        break;
                }
                break;
                
            case 2:
                show_menu_sub(menu_main);
                scanf("%d", &menu_sub);
                
                printf("\nSearch : ");
                scanf("%s", temp);
                switch(menu_sub) {
                    case 1:
                        print_filter_artist(data_list, temp);
                        break;
                    case 2:
                        print_filter_title(data_list, temp);
                        break;
                    case 3:
                        print_filter_genre(data_list, temp);
                        break;
                }
                break;
            case 3:
                printf("Input(Ex:PSY Gentleman Dance 2013)\n->");
                scanf("%s %s %s %d", data.artist, data.title, data.genre, &data.year);
                data.rating = data.vote = 0;
                add_data_struct(&data_list, data);
                break;
            case 4:
                printf("Input Title : ");
                scanf("%s", temp);
                add_rating(data_list, temp);
                break;
            default:
                break;
        }
    }
    
    return 0;
}

void show_menu_main(void) {
    printf("\n\n");
    printf("1. List All Data\n");
    printf("2. Search Record\n");
    printf("3. Add Record\n");
    printf("4. Add Ratings\n");
    printf("5. Exit\n\n");
    printf("Input : ");
}

void show_menu_sub(int menu) {
    
    if(menu >= 3) return;
    
    printf("\n");
    switch(menu) {
        case 1:
            printf("\t1. Order by Artists (Ascending order)\n");
            printf("\t2. Order by Title (Ascending order)\n");
            printf("\t3. Order by Genre (Descending order)\n");
            printf("\t4. Order by Year (Ascending order)\n");
            printf("\t5. Order by Rating (Descending order)\n\n");
            printf("\tInput : ");
            break;
        case 2:
            printf("\t1. By Artists\n");
            printf("\t2. By Title\n");
            printf("\t3. By Genre\n");
            printf("\tInput : ");
            break;
    }
}